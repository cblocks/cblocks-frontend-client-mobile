﻿using System;
using System.IO;
using System.Reflection;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace CBlocksUITests
{
    public static class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {

            if (platform == Platform.Android)
            {
                string currentFile = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                FileInfo fI = new FileInfo(currentFile);
                String directory = fI.Directory.Parent.Parent.Parent.FullName;
                var apkPath = Path.Combine(directory,"CBlocks", "CBlocks.Android", "bin", "Release", "com.companyname.CBlocks.apk");
                return ConfigureApp
                       .Android
                       .EnableLocalScreenshots()
                       .ApkFile(apkPath)
                       .StartApp();
            }
            return ConfigureApp.iOS.StartApp();
        }
    }
}