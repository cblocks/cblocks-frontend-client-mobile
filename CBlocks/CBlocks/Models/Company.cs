﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CBlocks.Models
{
    public class Company
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public List<Job> Jobs { get; set; }
        public Company()
        {
            Jobs = new List<Job>();
        }

    }
}
