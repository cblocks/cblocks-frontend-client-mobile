﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CBlocks.Models
{
    public class Job
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public Company Company { get; set; }
        public DateTime Posted { get; set; }
        public DateTime Deadline { get; set; }
        public string Desc { get; set; }
        public List<Requirement> Requirements { get; set; }

        public Job()
        {
            this.Requirements = new List<Requirement>();
        }
    }
}
