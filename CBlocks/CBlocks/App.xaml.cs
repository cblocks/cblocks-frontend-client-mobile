﻿using RichardSzalay.MockHttp;
using System;
using Xamarin.Forms;

namespace CBlocks
{
    public partial class App : Application
    {
        public static RestController AppRestController { get; private set; }
        private static String restURL = "https://api.cblocks.simonbreiter.com";
        private static String fragment = "/rest";
        public App()
        {
            InitializeComponent();

            Uri restURI = new Uri(restURL);
            restURI = new Uri(restURI, fragment);
            AppRestController = new RestController(restURI);

            //Start up the mainpage
            MainPage = new TabbedMain();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
