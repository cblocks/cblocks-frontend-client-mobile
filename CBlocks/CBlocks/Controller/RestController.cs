﻿using CBlocks.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace CBlocks
{
    public class RestController : Controller.IRestController
    {
        public HttpClient _client { get; private set; }

        // URL Fragments to point to the responsible resource
        private readonly string CompanyFragment = "/company/";
        private readonly string JobFragment = "/job/";
        private readonly string RequirementFragment = "/requirement/";
        public Uri RestUri { get; private set; }
        public Dictionary<string, Job> Jobs { get; private set; }
        public Dictionary<string, Company> Companies { get; private set; }
        /// <summary>
        /// Rest Controller 
        /// </summary>
        /// <param name="uri">The base uri to the rest api: http://example.com/rest </param>
        public RestController(Uri uri)
        {
            _client = new HttpClient();
            this.RestUri = uri;
            Jobs = new Dictionary<string, Job>();
            Companies = new Dictionary<string, Company>();
        }

        public async Task<Company> GetCompanyDetails(string Id)
        {
            var CompanyDetails = new Company();
            Uri query = new Uri(RestUri.ToString() + CompanyFragment + Id);

            try
            {
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var values = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(values);
                    CompanyDetails.ID = Id;
                    CompanyDetails.Name = json.Name;

                    if (Companies.ContainsKey(Id))
                    {
                        CompanyDetails.Jobs = Companies[Id].Jobs;
                    }

                    Companies.Add(CompanyDetails.ID, CompanyDetails);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e.Message);
            }
            return CompanyDetails;
        }

        public async Task<Job> GetJobDetails(string Id)
        {
            var JobDetails = new Job();
            Uri query = new Uri(RestUri.ToString() + JobFragment + Id);
            try
            {
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var values = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(values);
                    JobDetails = await ParseJobAsync(json);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e.Message);
            }
            return JobDetails;

        }

        public async Task<List<Company>> RefreshCompaniesAsync()
        {
            var CompanyList = new List<Company>();
            Uri query = new Uri(RestUri.ToString() + CompanyFragment);
            try
            {
                var response = await _client.GetAsync(query);

                if (response.IsSuccessStatusCode)
                {
                    var values = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(values);
                    foreach (var item in json)
                    {
                        Company comp = new Company
                        {
                            ID = item.id,
                            Name = item.Name
                        };
                        if (!Companies.ContainsKey(comp.ID))
                        {
                            Companies.Add(comp.ID, comp);

                        }
                        CompanyList.Add(comp);
                    }

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e.Message);
            }
            return CompanyList;
        }

        public async Task<List<Job>> RefreshJobsAsync()
        {
            var JobList = new List<Job>();
            Uri query = new Uri(RestUri.ToString() + JobFragment);
            try
            {
                //Parsing the json
                var response = await _client.GetAsync(query);
                if (response.IsSuccessStatusCode)
                {
                    var values = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(values);

                    foreach (var item in json)
                    {
                        if (!Jobs.ContainsKey((string)item.id))
                        {
                            Job job = await ParseJobAsync(item);
                            JobList.Add(job);
                        }
                        else
                        {
                            JobList.Add(Jobs[(string)item.id]);
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e.Message);
            }
            return JobList;
        }
        public async Task<Requirement> GetRequirementDetails(string Id)
        {
            Uri query = new Uri(RestUri.ToString() + RequirementFragment + Id);
            Requirement req = new Requirement();
            try
            {
                var response = await _client.GetAsync(query);

                if (response.IsSuccessStatusCode)
                {
                    var values = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(values);

                    req = new Requirement
                    {
                        ID = json.id,
                        RequirementName = json.Requirement
                    };

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e.Message);
            }
            return req;
        }
        //For mocking
        public void SetHttpClient(HttpClient httpClient)
        {
            _client = httpClient;
        }
        private async Task<Job> ParseJobAsync(dynamic item)
        {
            Job job = new Job
            {

                ID = item.id,
                Title = item.Title,
                Posted = Convert.ToDateTime(item.Posted),
                Deadline = Convert.ToDateTime(item.Deadline)

            };
            foreach (string reqID in item.Requirements)
            {
                var req = await GetRequirementDetails(reqID);
                job.Requirements.Add(req);
            }

            if (Companies.ContainsKey((string)item.Company))
            {
                job.Company = Companies[(string)item.Company];
            }
            else
            {
                var comp = GetCompanyDetails((string)item.Company);
                comp.Wait();
                job.Company = comp.Result;

            }
            Jobs.Add(job.ID, job);
            return job;
        }

    }
}
