﻿using CBlocks.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CBlocks.Controller
{
    interface IRestController
    {
        Task<List<Job>> RefreshJobsAsync();
        Task<List<Company>> RefreshCompaniesAsync();
        Task<Job> GetJobDetails(string Id);
        Task<Company> GetCompanyDetails(string Id);
        Task<Requirement> GetRequirementDetails(string Id);
    }
}
