﻿using CBlocks.Models;
using CBlocks.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CBlocks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OverviewPage : ContentPage
    {
        public OverviewPage()
        {
            InitializeComponent();
            listView.RefreshCommand = new Command(async () =>
          {
              listView.ItemsSource = await App.AppRestController.RefreshJobsAsync();
              listView.IsRefreshing = false;
          });
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await App.AppRestController.RefreshCompaniesAsync();
            listView.ItemsSource = await App.AppRestController.RefreshJobsAsync();
        }
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            await Navigation.PushAsync(new JobDetailPage
            {
                BindingContext = e.SelectedItem as Job
            });

        }
    }
}