﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace CBlocks
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedMain : Xamarin.Forms.TabbedPage
    {
        public TabbedMain()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
            BarBackgroundColor = Color.White;
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetBarItemColor(Color.Gray);

            On<Xamarin.Forms.PlatformConfiguration.Android>().SetBarSelectedItemColor(Color.LightBlue);
        }
    }
}