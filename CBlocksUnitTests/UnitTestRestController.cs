using NUnit.Framework; 
using System.Net;
using RichardSzalay.MockHttp;
using CBlocks;
using System;
using CBlocks.Models;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        [SetUp]

        [Test]
        public void CheckSetHttpClient()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/").Respond("text/html", "test");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController._client.GetAsync("http://localhost/rest/");
            task.Wait();
            var test = task.Result;
            var valTask = test.Content.ReadAsStringAsync();
            valTask.Wait();

            Assert.AreEqual("test", valTask.Result);
        }
        [Test]
        public void CheckJobSerialization()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/job/").Respond("application/json", "[{'id':1,'Title':'Programmier - Spezi','Company':1,'Posted':'2019 - 03 - 30','Deadline':'2019 - 05 - 30','Requirements':[5,7]},{'id':2,'Title':'Web Bastler','Company':1,'Posted':'2019 - 03 - 30','Deadline':'2019 - 05 - 30','Requirements':[6]}]");
            mockHTTP.When("http://localhost/rest/company/1").Respond("application/json", "{'id':'1','Name':'Houdini Inc'}");
            mockHTTP.When("http://localhost/rest/company/").Respond("application/json", "[{'id':'1','Name':'Houdini Inc'}]");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController.RefreshJobsAsync();
            task.Wait();
            var result = task.Result;
            Assert.AreEqual(2, result.Count);
        }
        [Test]
        public void CheckSingleJobSerialization()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/job/1").Respond("application/json", "{'id':1,'Title':'Programmier - Spezi','Company':1,'Posted':'2019 - 03 - 30','Deadline':'2019 - 05 - 30','Requirements':[5,7]}");
            mockHTTP.When("http://localhost/rest/company/1").Respond("application/json", "[{'id':'1','Name':'Houdini Inc'}]");
            mockHTTP.When("http://localhost/rest/company/").Respond("application/json", "[{'id':'1','Name':'Houdini Inc'}]");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController.GetJobDetails("1");
            task.Wait();
            Job result = task.Result;
            
            Assert.AreEqual("Programmier - Spezi", result.Title);

        }

        [Test]
        public void CheckCompanySerialization()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/company/").Respond("application/json", "[{'id':'1','Name':'Houdini Inc'}]");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController.RefreshCompaniesAsync();
            task.Wait();
            var result = task.Result;
            Assert.AreEqual(1, result.Count);
        }
        [Test]
        public void CheckSingleCompanySerialization()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/company/1").Respond("application/json", "{'id':'1','Name':'Houdini Inc'}");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController.GetCompanyDetails("1");
            task.Wait();
            var result = task.Result;
            Assert.AreEqual("Houdini Inc", result.Name);
        }

        [Test]
        public void CheckRequirementSerialization()
        {
            var mockHTTP = new MockHttpMessageHandler();
            mockHTTP.When("http://localhost/rest/requirement/1").Respond("application/json", "{'id':'1','Requirement':'Csharp'}");
            Uri restURI = new Uri("http://localhost");
            restURI = new Uri(restURI, "/rest");
            var restController = new RestController(restURI);
            restController.SetHttpClient(mockHTTP.ToHttpClient());
            var task = restController.GetRequirementDetails("1");
            task.Wait();
            var result = task.Result;
            Assert.AreEqual("Csharp", result.RequirementName);
        }
    }
}